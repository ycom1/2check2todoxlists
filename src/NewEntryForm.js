import { useState } from 'react';

export function NewEntryForm(props) {

    const [newItem, setNewItem] = useState("");

    function handleSubmit(e) {
      e.preventDefault();
      if (newItem === "") return;

      props.onSubmit(newItem);

      setNewItem("");
    }

    return(
    <>  
    <div className="container">
    <header>
      <form onSubmit={handleSubmit}>
      <input value={newItem} 
            onChange={e => setNewItem(e.target.value)}
            placeholder="entry" 
            id="inputField" 
            autoComplete="off"
            type="text"
            />
      <button className="addButton">add</button>
      </form>

      </header>
      </div>
    </>
    )
}