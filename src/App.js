import './App.css';
import { v4 as uuidv4 } from 'uuid';
import { NewEntryForm } from './NewEntryForm';
import { Entry } from "./Entry";

import { useState, useEffect } from 'react';


export default function App() {
    const [entries, setEntries] = useState( () => {
      const localentries = localStorage.getItem("entries");
      if (localentries == null) return[];
      return JSON.parse(localentries);
    });

    useEffect( () => {
      localStorage.setItem("entries", JSON.stringify(entries) );
    }, [entries] );


    function addNewItem(title) {
      console.log(title);
      if (title !== "") {
        setEntries(
          [...entries, { id:uuidv4(), title, completed: false } ]
        );
      }
    }

    function toggleentry(id, completed) {
      setEntries( currententries => {
        return currententries.map( entry => {
          if (entry.id === id) {
            return { ...entry, completed }
          }
          localStorage.setItem("entries", entries );
          return entry;
        })
      })
    }

    function deleteentry(id) {
      setEntries( currententries => {
        return currententries.filter( entry => entry.id !== id ) });
    }

    return( 
    <div>
        <NewEntryForm onSubmit={addNewItem} />
        <Entry entries={ entries } toggleentry={ toggleentry } deleteentry={ deleteentry } />
    </div>
    );
}


