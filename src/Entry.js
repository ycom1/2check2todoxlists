export function Entry( {entries, toggleentry, deleteentry } ){
return(
<div id="entryDiv">
        { entries.map( 
              entry =>  
              <div key={entry.id} className={`compl${entry.completed}`}>
                <div>
                  <input type="checkbox" 
                         onChange={e => toggleentry( entry.id, e.target.checked )} 
                         checked={entry.completed}
                         className={`compl${entry.completed}`}
                  />
                </div>
                <div><span>{entry.title}</span></div>
                <div><button 
                        onClick={() => deleteentry(entry.id)} 
                        className="deleteBtn">
                  X</button>
                </div>
              </div> 
        )}
</div>
);
};
